defmodule MyApp.Wallet do
  use Ecto.Schema
  import Ecto.Changeset
  alias MyApp.Wallet

  @primary_key {:address, :string, autogenerate: false}
  @derive {Phoenix.Param, key: :address}

  schema "wallets" do
    field :balance, :decimal, default: 0, precision: 26  # in Wei

    # associations
    has_many :deposits, MyApp.Deposit, foreign_key: :address
    has_many :withdrawals, MyApp.Withdrawal, foreign_key: :address

    timestamps()
  end


  @doc false
  def changeset(%Wallet{} = wallet, attrs) do
  wallet
  |> cast(attrs, [:address, :balance])
  |> validate_required(:address)
  |> unique_constraint(:address)

  end

end

