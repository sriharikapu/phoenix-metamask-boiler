defmodule MyApp.Util do
  require Logger
  alias Enum
  import ConCache


  def ecto_datetime_to_unix(nil), do: nil
  def ecto_datetime_to_unix(ecto_datetime) do
    ecto_datetime
    |> Ecto.DateTime.to_erl
    |> Calendar.DateTime.from_erl!("Etc/UTC")
    |> Calendar.DateTime.Format.unix
  end

  def get_list(values) when values in [nil, ""], do: []
  def get_list(values) do
    values
    |> String.split(",", trim: true)
  end

  def validate(walletAddress) do
    cond do
      walletAddress -> {:valid, walletAddress}
      !walletAddress -> {:error, walletAddress}
    end
  end

  # currency should be a string, one of the following: ""AUD", "BRL", "CAD", "CHF", "CLP", "CNY", "CZK", "DKK", "EUR", "GBP", "HKD", "HUF", "IDR", "ILS", "INR", "JPY", "KRW", "MXN", "MYR", "NOK", "NZD", "PHP", "PKR", "PLN", "RUB", "SEK", "SGD", "THB", "TRY", "TWD", "ZAR""
  def getCMCEthPrice(currency) do
    {:ok, %HTTPoison.Response{body: body}} = HTTPoison.get("https://api.coinmarketcap.com/v1/ticker/ethereum/?convert=" <> currency)
    body
    |> Poison.decode!
    |> List.first()
    |> Map.fetch!("price_" <> currency)
  end

  # default is USD
  def getCMCEthPrice() do
    {:ok, %HTTPoison.Response{body: body}} = HTTPoison.get("https://api.coinmarketcap.com/v1/ticker/ethereum/")
    body
    |> Poison.decode!
    |> List.first()
    |> Map.fetch!("price_usd")
  end

  def putCurrencyConversionsInCache() do
    usd_price = getCMCEthPrice()
    ConCache.put(:ethconversionprices, "usd", usd_price)
  end

end