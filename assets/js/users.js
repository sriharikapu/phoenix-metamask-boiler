import Wallet from "./wallet";

let Users = {

  init(socket){  //only initialized if web3 is found
		socket.connect();
    this.onReady(socket);
  },

	onReady(socket){
		let myappChannel = socket.channel("myapp:users");

		myappChannel.join()
			.receive("ok", resp => { console.log("Joined successfully", resp) })
			.receive("error", resp => { console.log("Unable to join", resp) });

		myappChannel.on("checkForNewWallet", ({msg}) => {
			this.updateWallet(socket);
		}),
		// resp will be of form {msg, status}
		myappChannel.on("user_alert", (resp) => {
			console.log("user alert: ", resp);
			this.publishAlert(resp);
		})
	},
	
	//
	// FUNCTIONS
	//

	updateWallet(socket, myappChannel){	
		if ((typeof socket.params["walletAddress"] === 'undefined' || null) || (typeof web3.eth.accounts[0] === 'undefined' || null)) {
			window.MetaMaskBool = false;
			console.log("MetaMask not logged in, walletAddress not found");
		}
		var oldWallet = socket.params["walletAddress"];
		var activeWallet = web3.eth.accounts[0];
		console.log("active wallet is: ", activeWallet);
		this.updateUI(window.MetaMaskBool) 
		// Swap to new wallet?
		if (activeWallet !== oldWallet) {
			let networkName = this.getEthNetworkName();
			socket.params["walletAddress"] = activeWallet;
			socket.channel("wallet:" + oldWallet).leave(); 	// leave old Wallet channel
			
			if (typeof web3.eth.accounts[0] !== 'undefined' || null){
				Wallet.init(socket, activeWallet, networkName);  // join new Wallet channel
				window.MetaMaskBool = true;
			} 
		// Update the UI with new MetaMask wallet state
			this.updateUI(window.MetaMaskBool)
		}
	},  

	getEthNetworkName() {
		let networkName = web3.version.network;
		return networkName;
	},

	//changes 3 things in Header
	updateUI(MetaMaskBool) {
		if (MetaMaskBool === true){
			document.getElementById("metamask-text").innerHTML='<div style="color:#01AA1D">MetaMask</div>'
			document.getElementById('deposit-button').disabled = false;
			document.getElementById('withdraw-button').disabled = false;
			
			
		} else {
			document.getElementById("metamask-text").innerHTML='<a href="https://metamask.io" target="_blank"><div style="color:#B80E0E; text-align:center">MetaMask</div></a>'
			document.getElementById('deposit-button').disabled = true;
			document.getElementById('withdraw-button').disabled = true;
			
		}
	},

	publishAlert({message}) {
    document.getElementById('transaction-notifications').innerHTML = '<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+ message + '</div>';
  },
}
export default Users
