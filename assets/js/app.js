// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import 'phoenix_html';

import $ from "jquery"
import "jquery"
import "bootstrap-select"
global.jQuery = require("jquery")
global.bootstrap = require("bootstrap")

//Import local files
import MainView from './views/main';
import socket from "./socket";
import Users from "./users";
import Wallet from "./wallet";

function handleDOMContentLoaded() {
  const view = new MainView();
  view.mount();
  window.currentView = view;
}
function handleDocumentUnload() {
  window.currentView.unmount();
}
window.addEventListener('DOMContentLoaded', handleDOMContentLoaded, false);
window.addEventListener('unload', handleDocumentUnload, false);

// Check web3 and find out what network we're on, then start the Channels
window.addEventListener('load', function() {
    // Check if Web3 has been injected by the browser:
  if (typeof web3 !== 'undefined' && typeof web3 !== 'loading') {
    window.web3 = new Web3(web3.currentProvider);
    window.MetaMaskBool = true;
    Users.init(socket);  //joins myapp:users channel, which will then start a wallet:* channel
  }
  else {
    window.MetaMaskBool = false;
    console.log("web3 provider problem");
    Users.updateUI(MetaMaskBool);
    document.getElementById('transaction-notifications').innerHTML='<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Welcome to MyApp. Without the MetaMask browser extension you can not do anything, so please install the Metamask Chrome Browser extension.</div>';
  }  
})


